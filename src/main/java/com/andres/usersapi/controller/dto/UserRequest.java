package com.andres.usersapi.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {

    @NotBlank(message = "El campo es obligatorio")
    private String name;

    @NotBlank(message = "El campo es obligatorio")
    @Pattern(regexp = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}", message = "Formato incorrecto para email. ejemplo: 'xxxxx@domain.xxx' ")
    private String email;

    @NotBlank(message = "El campo es obligatorio")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$", message = "La contraseña debe tener al menos 8 caracteres y debe contener:, 1 simbolo '!@#&()–[{}]:;',?/*~$^+=<>', 1  número, 1 letra minuscula y 1 letra mayuscula ")
    private String password;

    @Valid
    private List<PhoneRequest> phones;
}
