package com.andres.usersapi.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.LowerCaseStrategy.class)
public class PhoneRequest {

    @NotBlank(message = "El campo es obligatorio")
    private String number;

    @NotBlank(message = "El campo es obligatorio")
    private String cityCode;

    @NotBlank(message = "El campo es obligatorio")
    private String contryCode;

}
