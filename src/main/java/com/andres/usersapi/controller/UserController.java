package com.andres.usersapi.controller;

import com.andres.usersapi.config.exceptions.CustomException;
import com.andres.usersapi.controller.dto.UserRequest;
import com.andres.usersapi.controller.dto.UserResponse;
import com.andres.usersapi.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<UserResponse> create(@RequestBody @Valid UserRequest request) throws CustomException {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(request));
    }

}
