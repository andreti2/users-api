package com.andres.usersapi.entity.mapper;

import com.andres.usersapi.controller.dto.PhoneRequest;
import com.andres.usersapi.entity.PhoneEntity;
import com.andres.usersapi.entity.UserEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PhoneMapper {

    public static List<PhoneEntity> dtoToEntity(List<PhoneRequest> phonesRequestList, UserEntity userEntity) {

        List<PhoneEntity> phoneEntityList = new ArrayList<>();
        if (phonesRequestList.isEmpty()) return phoneEntityList;

        return phonesRequestList.stream().map(e ->
                PhoneEntity.builder().user(userEntity).number(e.getNumber()).cityCode(e.getCityCode()).contryCode(e.getContryCode()).build()
        ).collect(Collectors.toList());
    }

}
