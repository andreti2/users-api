package com.andres.usersapi.entity.mapper;

import com.andres.usersapi.controller.dto.UserRequest;
import com.andres.usersapi.controller.dto.UserResponse;
import com.andres.usersapi.entity.PhoneEntity;
import com.andres.usersapi.entity.UserEntity;
import com.andres.usersapi.util.Constants;

import java.util.stream.Collectors;

public class UserMapper {

    public static UserResponse entityToDto(UserEntity userEntity) {

        return UserResponse.builder().id(userEntity.getId())
                .createDate(Constants.formatDate(userEntity.getCreateDate()))
                .token(userEntity.getToken())
                .lastLogin(Constants.formatDate(userEntity.getLastLoginDate()))
                .modifiedDate(Constants.formatDate(userEntity.getModifiedDate()))
                .isActive(userEntity.getIsActive())
                .build();
    }

    public static UserEntity dtoToEntity(UserRequest userRequest, String token) {

        return UserEntity.builder().name(userRequest.getName())
                .email(userRequest.getEmail())
                .password(userRequest.getPassword())
                .token(token)
                .build();
    }
}
