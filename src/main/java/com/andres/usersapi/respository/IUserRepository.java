package com.andres.usersapi.respository;

import com.andres.usersapi.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IUserRepository extends JpaRepository<UserEntity, Long> {

    //List<UserEntity> findByNameContainingIgnoreCase(String name);

    //List<UserEntity> findByNameIgnoreCase(String name);
    Optional<UserEntity> findByEmail(String email);

}
