package com.andres.usersapi.respository;

import com.andres.usersapi.entity.PhoneEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPhoneRepository extends JpaRepository<PhoneEntity, Long> {
}
