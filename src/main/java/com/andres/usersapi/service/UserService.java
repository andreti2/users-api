package com.andres.usersapi.service;

import com.andres.usersapi.config.exceptions.CustomException;
import com.andres.usersapi.controller.dto.UserRequest;
import com.andres.usersapi.controller.dto.UserResponse;
import com.andres.usersapi.entity.PhoneEntity;
import com.andres.usersapi.entity.UserEntity;
import com.andres.usersapi.entity.mapper.PhoneMapper;
import com.andres.usersapi.entity.mapper.UserMapper;
import com.andres.usersapi.respository.IPhoneRepository;
import com.andres.usersapi.respository.IUserRepository;
import com.andres.usersapi.util.Constants;
import com.andres.usersapi.util.JwtTokenUtilService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final IUserRepository iUserRepository;
    private final IPhoneRepository iPhoneRepository;

    private final JwtTokenUtilService jwtTokenUtilService;

    public UserService(IUserRepository iUserRepository, IPhoneRepository iPhoneRepository, JwtTokenUtilService jwtTokenUtilService){
        this.iUserRepository = iUserRepository;
        this.iPhoneRepository = iPhoneRepository;
        this.jwtTokenUtilService = jwtTokenUtilService;
    }

    public UserResponse createUser(UserRequest request) throws CustomException{

        emailExist(request.getEmail());

        String token = jwtTokenUtilService.getJWTToken(request.getEmail());

        UserEntity newUserEntity = UserMapper.dtoToEntity(request, token);
        newUserEntity = iUserRepository.saveAndFlush(newUserEntity);

        List<PhoneEntity> phoneEntityList = PhoneMapper.dtoToEntity(request.getPhones(), newUserEntity);
        savePhonesUser(phoneEntityList);


        return UserMapper.entityToDto(newUserEntity);
    }

    private void savePhonesUser(List<PhoneEntity> phones){
        iPhoneRepository.saveAllAndFlush(phones);
    }

    private void emailExist(String email) throws CustomException {
        if(iUserRepository.findByEmail(email).isPresent())
            throw new CustomException(Constants.EMAIL_EXIST_ERROR_MSG, HttpStatus.BAD_REQUEST);
    }



}
