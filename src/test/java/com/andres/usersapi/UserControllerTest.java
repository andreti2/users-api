package com.andres.usersapi;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@SpringBootTest
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private String url = "http://localhost:8080/users";

    private String bodyRequestOk = "{\n" +
            "    \"name\": \"name person\",\n" +
            "    \"email\": \"aasf@sdasd.com\",\n" +
            "    \"password\": \"P@ast1234\",\n" +
            "    \"phones\": [\n" +
            "        {\n" +
            "            \"number\": \"3183234712\",\n" +
            "            \"citycode\": \"01\",\n" +
            "            \"contrycode\": \"57\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    private String bodyBadRequest = "{\n" +
            "    \"email\": \"aasf@sdasd.com\",\n" +
            "    \"password\": \"P@ast1234\",\n" +
            "    \"phones\": [\n" +
            "        {\n" +
            "            \"number\": \"3183234712\",\n" +
            "            \"citycode\": \"01\",\n" +
            "            \"contrycode\": \"57\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    @Test
    public void createOk() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(bodyRequestOk)
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createBadRequest() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(bodyBadRequest)
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
