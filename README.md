## EVALUACIÓN JAVA

# Entorno
- java 11
- h2
- gradle
- springboot


# Curl: Crear usuario

curl --location 'localhost:8080/users' \
--header 'Content-Type: application/json' \
--data-raw '{
"name": "Pepito Perez",
"email": "username@domain.com",
"password": "P@ast1234",
"phones": [
{
"number": "3183234712",
"citycode": "01",
"contrycode": "57"
},
{
"number": "3183234714",
"citycode": "01",
"contrycode": "57"
}
]
}'